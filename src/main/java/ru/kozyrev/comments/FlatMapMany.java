package ru.kozyrev.comments;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FlatMapMany {

    public static void main(String[] args) {
        System.out.println();System.out.println("Mono flatMapMany");System.out.println("----------------------");

        Mono<String> mono = Mono.just("Data");

        // Метод flatMapMany() превращает Mono во Flux.
        Flux<Object> objectFlux = mono.flatMapMany(elem -> Flux.just(elem));
        objectFlux.subscribe(System.out::println);
    }
}

