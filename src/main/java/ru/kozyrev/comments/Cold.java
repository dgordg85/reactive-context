package ru.kozyrev.comments;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Stream;

public class Cold {

    private static Stream<String> getMovie() {
        System.out.println("Start streaming...");
        return Stream.of(
                "thread 1",
                "thread 2",
                "thread 3",
                "thread 4",
                "thread 5"
        );
    }

    public static void main(String[] args) throws InterruptedException {
        //Например, HTTP-запрос: каждый новый подписчик запускает HTTP-вызов, но он не выполняется,
        // если никто не заинтересован в результате (пока никто на него не подпишется).
        //Например, представьте себе Netflix. Netflix не транслирует фильмы самостоятельно, когда никто не смотрит.
        // Если вы вошли на сайт Netflix и хотели посмотреть свой любимый фильм, вы можете начать смотреть его в любое время.
        // Многие люди могут смотреть один и тот же фильм из разных мест. Один человек мог смотреть фильм в течение часа,
        // а другой мог только начать смотреть данный фильм.
        //По умолчанию издатели не производят никакого значения, если на него не подписан хотя бы один наблюдатель.
        // Издатели создают новых производителей данных для каждой новой подписки.

        Flux<String> streamingVideo = Flux.fromStream(() -> getMovie())
                .delayElements(Duration.ofSeconds(2));
        // new Thread().setDaemon(true);
        // Первый человек начал смотреть фильм "Titanic"
        streamingVideo.subscribe(scene -> System.out.println("First person is watching " + scene));

        // Второй человек начал смотреть фильм "Titanic" спустя 5 секунд
        Thread.sleep(5000);
        streamingVideo.subscribe(scene -> System.out.println("Second person is watching " + scene));
        Thread.sleep(15000);

        //Start streaming...
        //First person is watching thread 1
        //First person is watching thread 2
        //Start streaming...
        //First person is watching thread 3
        //Second person is watching thread 1
        //First person is watching thread 4
        //Second person is watching thread 2
        //First person is watching thread 5
        //Second person is watching thread 3
        //Second person is watching thread 4
        //Second person is watching thread 5

        //Здесь из вывода мы можем понять, что каждая новая подписка запускает запрос getMovie().
        //Вот почему мы дважды видим в выводе «Start streaming…».
        //Первый человек начал смотреть фильм Титаник первым. Еще до того, как второй человек начал смотреть фильм,
        // первый человек уже закончил смотреть 2 сцены в этом фильме.
        //Эти люди смотрят один и тот же фильм параллельно, но разные сцены. Второй человек не потерял ни одной сцены т
        // олько потому, что присоединился чуть позже (спустя 5 секунд).
        //Наш сервис принял запрос от второго человека и начал заново транслировать сцены только для него.
    }
}
