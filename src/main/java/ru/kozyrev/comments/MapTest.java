package ru.kozyrev.comments;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MapTest {
    public static void main(String[] args) {

        // Создание потока содержащего элементы
        Mono<String> mono = Mono.just("case");
        Flux<Integer> flux = Flux.just(1, 2, 3, 4);

        // Flux и Mono реализуют Publisher интерфейс
        Publisher<String> mono2 = Mono.just("case2");
        Publisher<Integer> flux2 = Flux.just(15, 28, 43, 54);

        // Map примененяется к стриму Stream<T> и возвращает Stream<R>.
        // Операция map создает одно выходное значение для каждого входного значения.

        // Map возвращает новый поток, основанный на родительском.
        // Функции не модифицируют родительский поток. Это называется неизменяемостью и является
        // неотъемлемой частью реактивного подхода, позволяя нам вызывать цепочку функций.

        // Map принимает в качестве аргумента синхронную Function(например, лямбду),
        // которая вызывается для каждого значения входного стрима(<T>),
        // преобразует это значение в другое значение, и посылает получившееся значение в выходной стрим(который <R>).

        // Т.е. map для каждого объекта в стриме возвращает по 1 объекту, потом преобразует все объекты в итоговый стрим.
        System.out.println(); System.out.println("Mono map"); System.out.println("----------------------");

        mono.map(str -> str.toUpperCase())
                .subscribe(str -> System.out.print(str + " "));
        // OUTPUT: CASE

        System.out.println(); System.out.println(); System.out.println("Flux map"); System.out.println("----------------------");

        flux.map(elem -> elem * 2)
                .subscribe(integer -> System.out.print(integer + " "));
        // OUTPUT: 2 4 6 8

        System.out.println();
    }
}
