package ru.kozyrev.comments;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

import java.util.HashMap;
import java.util.Map;

public class ContextAPI {

    // Существует такое понятие как Context API.
    // Context - это интерфейс, напоминающий Map: он хранит пары ключ-значение и позволяет получить значение,
    // которое сохранили по ключу.
    // Ключом и значением может быть любой объект.
    // Contex является потокобезопасным и имутабельным.

    public static void main(String[] args) {


        // Вы можете создать пустой контекст с помощью
        Context emptyContext = Context.empty();

        // Context изначально проектировался как неизменяемый объект и после добавления в него нового элемента мы
        // получим новый экземпляр Context. Это единственный способ передать Context с потоком данных и иметь
        // возможность динамически добавлять в него информацию, которая должна быть доступна в течение всего этапа
        // сборки или подписки.

        //Метод put() — чтобы положить ключ/значение.
        Context ourContext = Context.of("anyKey", "anyValue");
        Context newContext = ourContext.put("mainKey", "value");

        //Метод hasKey() — позволяет проверить наличие ключа.
        System.out.println(newContext.hasKey("mainKey"));
        //OUTPUT: TRUE

        //Метод getOrEmpty() — получить пустое значение, если у контекста нет этого ключа.
        System.out.println(newContext.getOrEmpty("empty"));
        //OUTPUT: Optional.empty

        //Метод delete() — удаляет значение, связанное с ключом
        Context deleteContext = newContext.delete("anyKey");

        // Именно поэтому нежелательно просто взять и добавить в сам Context что-либо.
        // Для этого лучше внутри Context использовать map и уже в эту map добавить значения.

        Map<String, Context> map = new HashMap<>();
        map.put("MDC_CONTEXT_MAP", newContext);

        Context mapContext = Context.of(map);

        // Context может добавляться в поток данных с использованием оператора subscriberContext(Context).
        Flux.just("Hubert", "Sharon")
                .flatMap(name -> nameToGreeting(name))
                .subscriberContext(context -> Context.of("greetingWord", "Hello"))
                .subscribe(str -> System.out.println(str));

        // OUTPUT:
        // Hello Hubert !!!
        // Hello Sharon !!!
    }

    //Экземпляр Context доступен через оператора Mono.subscriberContext
    private static Mono<String> nameToGreeting(final String name) {
        return Mono.subscriberContext()
                .map(context -> context.get("greetingWord"))
                .flatMap(greetingWord -> Mono.just(greetingWord + " " + name + " " + "!!!"));
    }

    public static final String USERNAME = "username";

    @NotNull
    public Mono<Void> filter(@NotNull final ServerWebExchange exchange, @NotNull final WebFilterChain chain) {
        return Mono.subscriberContext().flatMap(context -> {
            final Map<String, String> mdc = context.get("MDC_CONTEXT_MAP");
            mdc.put(USERNAME, "anonymous");
            return chain.filter(exchange);
        });
    }
}
