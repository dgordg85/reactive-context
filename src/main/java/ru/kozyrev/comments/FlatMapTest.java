package ru.kozyrev.comments;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

public class FlatMapTest {

    private final String name;
    private final List<String> pets;

    public FlatMapTest(final String name, final List<String> pets) {
        this.name = name;
        this.pets = pets;
    }

    public List<String> getPets() {
        return pets;
    }

    // FlatMap может быть применен к стриму Stream<T> и возвращает также стрим Stream<R>.
    // Разница заключается в том, flatMap создает произвольное число(ноль или больше) значений
    // для каждого входного значения. т.е. она преобразует поток в плоскую структуру.

    //  Асинхронно преобразует элементы, испускаемые этим потоком, в Publisher-ы, а затем объединяет эти
    //  внутренние Publisher-ы в единый поток посредством слияния

    // Операция flatMap принимает функцию (которая преобразует каждое значение входного стрима в стрим),
    // применяет ее к каждому элементу, и на выходе возвращает стрим с одним, несколькими или ни c одним
    // из элементов для каждого элемента входящего стрима.

    // Т.е., flatMap возвращает по стриму для каждого объекта в первоначальном стриме, а затем результирующие
    // потоки объединяются в исходный стрим.

    // flatMap не гарантирует порядок элементов - для этого используется concatMap (но при этом увеличивается время обработки)

    // flatMap: не сохраняет порядок элементов, работает асинхронно
    // concatMap: сохранение порядка элементов, работает синхронно

    public static void main(String[] args) {

        System.out.println();System.out.println("Mono flatMap");System.out.println("----------------------");

        FlatMapTest flatMapTest = new FlatMapTest("Dan", Arrays.asList("Carry", "Bitty"));
        Mono<FlatMapTest> monoFlatMap = Mono.just(flatMapTest);
        monoFlatMap.flatMap(human -> Mono.just(human.getPets().get(0)))
                .subscribe(System.out::println);

        System.out.println();System.out.println("Flux flatMap");System.out.println("----------------------");

        List<FlatMapTest> humans = Arrays.asList(
                new FlatMapTest("Sam", Arrays.asList("Buddy", "Lucy")),
                new FlatMapTest("Bob", Arrays.asList("Frankie", "Rosie")),
                new FlatMapTest("Marta", Arrays.asList("Simba", "Tilly")));

        Flux<FlatMapTest> humanFlux = Flux.fromIterable(humans);

        Flux<String> petNames = humanFlux
                .flatMap(human -> Flux.fromIterable(human.getPets()));

        petNames
                .subscribe(s -> System.out.print(s + " "));

        // OUTPUT: Buddy Lucy Frankie Rosie Simba Tilly

        System.out.println();System.out.println();System.out.println("Flux flatMapIterable");System.out.println("----------------------");

        humanFlux
                .flatMapIterable(map -> map.getPets())
                .subscribe(s -> System.out.print(s + " "));

        // OUTPUT: Buddy Lucy Frankie Rosie Simba Tilly

        System.out.println();
    }

    //Предыдущее описание относится к холодной(Cold) семье издателей. Они генерируют данные заново для каждой подписки.
    // Если подписка не создана, данные никогда не создаются.
}
