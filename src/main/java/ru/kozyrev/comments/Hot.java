package ru.kozyrev.comments;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Stream;

public class Hot {

    public static void main(String[] args) throws InterruptedException {
        // Они не зависят от количества подписчиков. Они могут начать публиковать данные сразу же и будут
        // продолжать делать это всякий раз, когда появляется новый подписчик (в этом случае подписчик будет
        // видеть только новые элементы, испускаемые после его подписки, а старые, которые были выпущены
        // ДО уже не увидит).
        // Эти типы потоков называются горячими потоками, так как они всегда работают и на них можно подписаться
        // в любой момент времени, пропуская начало данных (Например, у нас может быть поток движений мыши, на
        // который нужно постоянно реагировать или твиттер).
        // По умолчанию Flux и Mono являются холодными издателями, что означает, что они генерируют новые данные
        // каждый раз, когда на них подписывается новый подписчик.

        //Вы можете преобразовать “холодного| издателя в “горячего”, вызвав .cache()

        Flux<String> flux = Flux.just("str1", "str2")
                .delayElements(Duration.ofSeconds(2))
                .cache();

        // Существует несколько других операторов для преобразования «холодного» издателя в «горячего» издателя и наоборот.
        // Оператор, который преобразует «горячего» издателя в «холодного» — just().

        // Например: у нас есть радиостанция. Неважно, действительно ли люди слушают радио, она всегда будет транслировать
        // песни или новости. Слушатели могут наблюдать за радиостанцией в любое время. Но все слушатели получают одинаковую
        // информацию в любой момент. Они получают новости или песни независимо от того, что транслируют в этот момент.

        // Горячие издатели не создают нового производителя данных для каждой новой подписки (как это делает холодный
        // издатель). Вместо этого будет только один производитель данных, и все наблюдатели будут слушать данные,
        // созданные одним производителем данных. Таким образом, все наблюдатели получают одинаковые данные.

        // Давайте теперь превратим наш сервис Netflix в радиостанцию ​​или кинотеатр. Здесь мы только что
        // добавили метод share() в наш Flux, чтобы превратить сервер Netflix в кинотеатр (сделали его из холодного — горячим).
        // Метод share() немедленно запускает источник и заставляет поздних подписчиков видеть более поздние элементы.

        Flux<String> radioStation = Flux.fromStream(() -> getAudioPodcast())
                .delayElements(Duration.ofSeconds(2)).share();
        // Thread.sleep(5000);

        // Первый человек слушает радио подкаст
         radioStation.subscribe(scene -> System.out.println("First person is listening " + scene));

        // Запустим самый первый горячий стрим через метод Cache
        // flux.subscribe(elem -> System.out.println("Hot Cache Stream " + elem));

        // Второй человек начал слушать радио подкаст спустя 5 секунд
        Thread.sleep(5000);
        radioStation.subscribe(scene -> System.out.println("Second person is listening " + scene));
        Thread.sleep(15000);

        //OUTPUT: Start streaming...
        //First person is listening thread 1
        //First person is listening thread 2
        //First person is listening thread 3
        //Second person is listening thread 3
        //First person is listening thread 4
        //Second person is listening thread 4
        //First person is listening thread 5
        //Second person is listening thread 5
    }

    private static Stream<String> getAudioPodcast(){
        System.out.println("Start streaming...");
        return Stream.of(
                "thread 1",
                "thread 2",
                "thread 3",
                "thread 4",
                "thread 5"
        );
    }
}
